<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Hello, ADSD!</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <h1 class="display-2"> Hello, ADSD!</h1>
            <div class="offset-1 col-10">
                <h1 class="display-3">Variables</h1>
                <div class="text-success">
                    <?php require
                    //SQR - separation of responsibility
                    "content.php";
                    ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Hier komt eerste php code -->
    <?php
   //     $greeting = "Hello, ADSD!"; //statements altijd afsluiten met een semi-colon
   //     $age = 23; //Number!

        //concatenation
   //     echo "<p>" .
   //         $greeting .
   //         "</p>
   //         <p>" .
   //             $age .
   //         "</p>"; //Door de punten kunnen we strings en variable samen koppelen

            //Single-Line commands
            /**
             * Multi-line commands
             **/

            //Gaat over Strings

    //        $myText = "Today it is a beautiful day, the sun is shining and I'm happy. \n Tomorrow I don't think the weather is going
    //        to be that great. \n I'm kind of upset about that, but that's okay!";
   //         //String function
   //         echo nl2br($myText);



    ?>
</body>
</html>

