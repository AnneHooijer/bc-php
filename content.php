<?php
//Dit stukje gaat over Array

//   Array key => value
$ages = [
    "Kim" => 7,
    "Piet" => 35,
    "Henk" => 46,
    "Oma" =>  77,
    "Opa" => 78
];
//If true dan naam laten zien - de naam is de key! anders "Wie is dit?"
$age = 46;

if(array_search($age, $ages)) {
    echo array_search($age, $ages) . " is " . $ages[array_search($age, $ages)] . " oud!";
} else {
    echo "wie ben jij";
}

